#!/usr/bin/env bash

docker run --name product-service -p 3000:3000 -d product-service \
-e ALLOW_INSECURE_CONNECTION=1