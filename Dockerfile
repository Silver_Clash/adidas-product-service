# Node v8 as the base image
FROM node:8.3.0
# New user for new container to avoid the root user
RUN useradd --user-group --create-home --shell /bin/false adidas_challenge && \
    apt-get clean
ENV HOME=/home/adidas_challenge

# DEVELOPMENT ENV VARIABLES - SHOULD BE REMOVED IN PRODUCTION BUILD
ENV ALLOW_INSECURE_CONNECTION=1
ENV REVIEW_SERVICE_URL="https://host.docker.internal:3001/review/"

COPY package.json $HOME/app/

COPY src/ $HOME/app/src
RUN chown -R adidas_challenge:adidas_challenge $HOME/* /usr/local/
WORKDIR $HOME/app
RUN npm cache verify && \
    npm install --silent --progress=false --production
RUN chown -R adidas_challenge:adidas_challenge $HOME/*
USER adidas_challenge
EXPOSE 3000
CMD ["npm", "start"]
