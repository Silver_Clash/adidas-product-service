#!/usr/bin/env bash

docker rm -f product-service

docker rmi product-service

docker image prune

docker volume prune

docker build -t product-service .