const argv = require('optimist').argv;
const status = require('http-status');
const supertest = require('supertest');

describe('Product Service', () => {
  const api = supertest('http://localhost:' + (argv.port || 3000));
  
  it('Can NOT find not existed product', (done) => {
    api.get('/product/never-existed-product')
      .expect(status.NOT_FOUND, done);
  });
  
  it('Can find known (C77154) product', (done) => {
    api.get('/product/C77154')
      .expect(status.OK)
      .expect((res) => {
        if (!res.body || res.body.id !== 'C77154'){
           throw new Error('Product id not match');
        }
      })
      .end(done);
  });
  
  it('Include reviews data for known (C77154) product', (done) => {
    api.get('/product/C77154')
      .expect(status.OK)
      .expect((res) => {
        if (!res.body || res.body.product_id !== 'C77154') {
          throw new Error('Review data not found. IS REVIEW SERVICE STARTED AND CONFIGURED?');
        }
      })
      .end(done);
  });
});