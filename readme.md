## Adidas coding challenge - Product service

### Stack

*   NodeJS 8.3.0
*   NeDB - as a simple persistent data store with Mongo - like API (used as in-memory cache with configurable TTL)

### how to run it

**This to run our microservice in a docker container** You need to create Docker image (see below) and run it

**To run in locally** You need nodejs installed

*   run integration (API) tests

        mocha integration-tests\index.js

*   run unit tests (jsut an example with 1 simple test)

        npm test

*   start the service in development mode (ignore insecure connection due to self-signed certificate error

        set ALLOW_INSECURE_CONNECTION=1 && node src\index.js

    OR

        npm start

*   Create docker image

        create-image.sh

*   Run docker image

        run-container.sh

    OR

        docker run --name product-service -p 3000:3000 -d product-service

### how to use it

**There is Postman collection available to simplify API testing**

*   Just check file:

        /integration-tests/Adidas - Product and Review Services.postman_collection.json