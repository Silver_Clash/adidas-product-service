'use strict';
const status = require('http-status');

module.exports = (app, options) => {
  const {repo} = options;

  app.get('/product/:id', (req, res, next) => {
    repo.getProductById(req.params.id).then(product => {
      //@TODO here will be added review field received from microservice;
      res.status(status.OK).json(product);
    }).catch(next);
  });
};