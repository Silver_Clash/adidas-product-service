const productService = {
  url: process.env.EXT_PRODUCTS_SERVICE_URL || 'https://www.adidas.co.uk/api/products/'
};

module.exports = Object.assign({}, { productService });