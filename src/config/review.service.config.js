const reviewService = {
  url: process.env.REVIEW_SERVICE_URL || 'https://docker.for.win.localhost:3001/review/',
  //this option should be used only for test purposes!
  insecure: process.env.ALLOW_INSECURE_CONNECTION
};

module.exports = Object.assign({}, { reviewService });