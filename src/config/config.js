/*
 * It is better (as a best practices talking) to NOT include configuration
 * file to repository to not show sensitive information about the project
 * and to not overwrite production config by mistake or occasion.
 * 
 * But we have just an test project and with to respect this,
 * configuration file is filled completely to not loose time on verification
 */

const dbSettings = {
  db: process.env.DB || 'products',
  dbParameters: {
    timestampData: true
  },
  ttl: process.env.TTL || 3600
};

const serverSettings = {
  port: process.env.PORT || 3000
};

module.exports = Object.assign({}, { dbSettings, serverSettings });