const Datastore = require('nedb');

const connect = (options, mediator) => {
  mediator.once('boot.ready', () => {
    const db = new Datastore(options.dbParameters);
    // Using ttl index to keep inmemory DB in actual state
    db.ensureIndex({ fieldName: 'createdAt', expireAfterSeconds: options.ttl }, (err) => {
      if (err) {
        mediator.emit('db.error', err);
      }
      mediator.emit('db.ready', db);
    });
  });
};


module.exports = Object.assign({}, {connect});