'use strict';
//@TODO keep in mind DI could be better
const services = require('../services/');
const status = require('http-status');

const repository = (db) => {
  const getProductById = (id) => {
    return new Promise((resolve, reject) => {
      
      const fetchProductFallback = (err, product) => {
        //@TODO maybe better to log this error and try to fetch product data from external service
        if (err) {
          reject(new Error(`An error occured fetching a product with id: ${id}, err: ${err}`));
        }
        if (!product) {
          services.productService
              .fetch(id)
              .then(product => {
                db.insert(product);
                sendProduct(product);
              })
              .catch(reject);
        } else {
          sendProduct(product);
        }
      };
      const options = {createdAt: 0, updatedAt: 0, _id: 0};
      db.findOne({id: id}, options, fetchProductFallback);
      
      const sendProduct = (product) => {
        services.reviewService
            .fetch(id)
            .then(review => {
              resolve(Object.assign({}, product, review));
            })
            .catch((err) => {
              // if review not found, or external service is offline
              // there is nothing to do, sending product without review data
              // it's not an error we can manage in our code, it's some kind of regular response,
              // we should define expected behaviuor so solve this issue
              
              
              console.log(err);
              
              if (err.statusCode === status.NOT_FOUND || err.error.code === 'ECONNREFUSED') {
                return resolve(product);
              }
              reject(err);
            });
      };
    }).catch((err) => {
      throw err;
    });
  };

  return Object.create({
    getProductById
  });
};

const connect = (connection) => {
  return new Promise((resolve, reject) => {
    if (!connection) {
      reject(new Error('connection db not supplied!'));
    }
    resolve(repository(connection));
  });
};

module.exports = Object.assign({}, {connect});