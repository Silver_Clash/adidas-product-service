const rp = require('request-promise');
const config = require('../config/review.service.config');
const baseUrl = config.reviewService.url;

const fetch = (pid) => {
  return new Promise((resolve, reject) => {
    
    const options = {
      uri: baseUrl + pid,
      json: true
    };
    
    //small workaround for test/dev environment purposes
    if (config.reviewService.insecure) {
      console.warn('WARNING: insecure connections allowed by environment!');
      options.rejectUnauthorized = false;
    }
    
    rp(options)
        .then((data) => {
          resolve(data);
        })
        .catch(reject);
  });
};


module.exports = Object.assign({}, {fetch});