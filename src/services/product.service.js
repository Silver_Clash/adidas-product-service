const rp = require('request-promise');
const config = require('../config/product.service.config');
const baseUrl = config.productService.url;

const fetch = (id) => {
  return new Promise((resolve, reject) => {
    
    const options = {
      uri: baseUrl + id,
      json: true
    };
    
    rp(options)
        .then((data) => {
          resolve(data);
        })
        .catch(reject);
  });
};


module.exports = Object.assign({}, {fetch});