const productService = require('./product.service');
const reviewService = require('./review.service');

module.exports = Object.assign({}, { productService, reviewService });