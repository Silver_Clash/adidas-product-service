const api = require('../api/products');
const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const status = require('http-status');

const start = (options) => {
  return new Promise((resolve, reject) => {
    if (!options.repo) {
      reject(new Error('The server must be started with a connected repository'));
    }
    if (!options.port) {
      reject(new Error('The server must be started with an available port'));
    }

    const app = express();
    app.use(morgan('dev'));
    app.use(helmet());

    api(app, options);
    
    app.use((req, res, next) => {
      //we are supporting only GET, so, other requests will get 405
      res.sendStatus(req.method !== 'GET' ? status.METHOD_NOT_ALLOWED : status.NOT_FOUND);
    });
    
    app.use((err, req, res, next) => {
      reject(new Error('Something went wrong!, err:' + err));
      res.sendStatus(err.statusCode);
    });

    const server = app.listen(options.port, () => resolve(server));
  });
};

module.exports = Object.assign({}, {start});